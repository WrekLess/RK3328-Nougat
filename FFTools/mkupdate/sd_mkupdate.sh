#!/bin/bash

set -e

usage()
{
cat << EOF
usage:
    $(basename $0) [update_img_name]
    -n: dest update.img name, if have not this arg, there are an default name, like:
        ROC-RK3328-CC_Android7.1_YYMMDD

NOTE: Run in the path of SDKROOT
EOF

if [ ! -z $1 ] ; then
    exit $1
fi
}

if [ "${1:0:1}" == "-" ]; then
    if [ "$1" == "-h" ]; then
        usage 0
    else
        usage 1
    fi
elif [ ! -z $1  ] ; then
    UPDATE_USER_NAME=$1
fi

. build/envsetup.sh >/dev/null && setpaths

#set -x
TARGET_PRODUCT=`get_build_var TARGET_PRODUCT`
echo -e "TARGET_PRODUCT=$TARGET_PRODUCT\n"
TARGET_VERSION=`get_build_var PLATFORM_VERSION`

CMD_PATH=$(dirname $0)
IMAGE_SRC_CUR_PATH=rockdev/Image-$TARGET_PRODUCT
IMAGE_SRC_PATH=$(readlink -f $IMAGE_SRC_CUR_PATH)
LINK_IMAGE_PATH=Image

if [ ! -d ${IMAGE_SRC_CUR_PATH} ] ; then
    echo "[ERROR]: Can't find image path: ${IMAGE_SRC_CUR_PATH}"
    exit 1
fi

pushd $CMD_PATH > /dev/null

ln -sf ${IMAGE_SRC_PATH} ${LINK_IMAGE_PATH}

if [ -z "$UPDATE_USER_NAME" ] ; then
    UPDADE_NAME_TMP="ROC-RK3328-CC_Android${TARGET_VERSION}_$(date -d today +%y%m%d)"
else
    UPDADE_NAME_TMP="$UPDATE_USER_NAME"
fi

UPDADE_NAME="${LINK_IMAGE_PATH}/${UPDADE_NAME_TMP}.img"

GENERATE_IDBLOCK_DATA()
{
    ./boot_merger --gensdboot ${LINK_IMAGE_PATH}/MiniLoaderAll.bin
}

FLASH_IMAGR_TO_PARTITION()
{
    ./rkcrc -p ${LINK_IMAGE_PATH}/parameter.txt ${LINK_IMAGE_PATH}/parameter.img

    dd if=${LINK_IMAGE_PATH}/parameter.img     of=${UPDADE_NAME} seek=$(((0x000000 + 0x2000))) ibs=1M conv=sync,fsync
    dd if=${LINK_IMAGE_PATH}/uboot.img         of=${UPDADE_NAME} seek=$(((0x002000 + 0x2000))) ibs=1M conv=sync,fsync
    dd if=${LINK_IMAGE_PATH}/trust.img         of=${UPDADE_NAME} seek=$(((0x004000 + 0x2000))) ibs=1M conv=sync,fsync
    dd if=${LINK_IMAGE_PATH}/misc.img          of=${UPDADE_NAME} seek=$(((0x008000 + 0x2000))) ibs=1M conv=sync,fsync
    dd if=${LINK_IMAGE_PATH}/resource.img      of=${UPDADE_NAME} seek=$(((0x00A800 + 0x2000))) ibs=1M conv=sync,fsync
    dd if=${LINK_IMAGE_PATH}/kernel.img        of=${UPDADE_NAME} seek=$(((0x012000 + 0x2000))) ibs=1M conv=sync,fsync
    dd if=${LINK_IMAGE_PATH}/boot.img          of=${UPDADE_NAME} seek=$(((0x022000 + 0x2000))) ibs=1M conv=sync,fsync
    dd if=${LINK_IMAGE_PATH}/recovery.img      of=${UPDADE_NAME} seek=$(((0x032000 + 0x2000))) ibs=1M conv=sync,fsync
    dd if=${LINK_IMAGE_PATH}/system.img        of=${UPDADE_NAME} seek=$(((0x0AC000 + 0x2000))) ibs=1M conv=sync,fsync

    rm -rf ${LINK_IMAGE_PATH}/parameter.img
	rm -f ${LINK_IMAGE_PATH}
}

FLASH_LOADER()
{
    GENERATE_IDBLOCK_DATA
    dd if=SD.bin	  of=${UPDADE_NAME} seek=$(((0x0040 + 0x0000))) conv=sync,fsync
    dd if=SD.bin      of=${UPDADE_NAME} seek=$(((0x0040 + 0x0400))) conv=sync,fsync
    dd if=SD.bin      of=${UPDADE_NAME} seek=$(((0x0040 + 0x0800))) conv=sync,fsync
    dd if=SD.bin      of=${UPDADE_NAME} seek=$(((0x0040 + 0x0C00))) conv=sync,fsync
    dd if=SD.bin      of=${UPDADE_NAME} seek=$(((0x0040 + 0x1000))) conv=sync,fsync

	rm -rf SD.bin
}

FLASH_LOADER
FLASH_IMAGR_TO_PARTITION

popd > /dev/null

#xz -k -z ${IMAGE_SRC_CUR_PATH}/${UPDADE_NAME}

echo ""
echo "Making updateimg: ${IMAGE_SRC_CUR_PATH}/${UPDADE_NAME}"

exit 0
