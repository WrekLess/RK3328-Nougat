#!/bin/bash

set -e

usage()
{
cat << EOF
usage:
    $(basename $0) [update_img_name]
    -n: dest update.img name, if have not this arg, there are an default name, like:
        ROC-RK3328-CC_Android7.1_YYMMDD

NOTE: Run in the path of SDKROOT
EOF

if [ ! -z $1 ] ; then
    exit $1
fi
}

if [ "${1:0:1}" == "-" ]; then
    if [ "$1" == "-h" ]; then
        usage 0
    else
        usage 1
    fi
elif [ ! -z $1  ] ; then
    UPDATE_USER_NAME=$1
fi

. build/envsetup.sh >/dev/null && setpaths

#set -x
TARGET_PRODUCT=`get_build_var TARGET_PRODUCT`
echo -e "TARGET_PRODUCT=$TARGET_PRODUCT\n"
TARGET_VERSION=`get_build_var PLATFORM_VERSION`

CMD_PATH=$(dirname $0)
IMAGE_SRC_CUR_PATH=rockdev/Image-$TARGET_PRODUCT
IMAGE_SRC_PATH=$(readlink -f $IMAGE_SRC_CUR_PATH)
LOADER_PATH=$(awk /bootloader/'{print $2}' ${CMD_PATH}/package-file | tr -d '\r\n')
LINK_IMAGE_PATH=$(echo ${LOADER_PATH} | awk -F/ '{print $1}')

if [ -z "$UPDATE_USER_NAME" ] ; then
    UPDADE_NAME="ROC-RK3328-CC_Android${TARGET_VERSION}_$(date -d today +%y%m%d)"
else
    UPDADE_NAME="$UPDATE_USER_NAME"
fi

if [ ! -d ${IMAGE_SRC_CUR_PATH} ] ; then
    echo "[ERROR]: Can't find image path: ${IMAGE_SRC_CUR_PATH}"
    exit 1
fi

pushd $CMD_PATH > /dev/null

ln -sf ${IMAGE_SRC_PATH} ${LINK_IMAGE_PATH}
if [ ! -f ${LOADER_PATH} ]; then
    echo "[ERROR]: Can't find loader: ${LOADER_PATH}"
    exit 2
fi

./afptool -pack ./ ${LINK_IMAGE_PATH}/tmp_${UPDADE_NAME}.img
./rkImageMaker -RK322H ${LOADER_PATH} ${LINK_IMAGE_PATH}/tmp_${UPDADE_NAME}.img "${LINK_IMAGE_PATH}/${UPDADE_NAME}.img" -os_type:androidos
rm -f ${LINK_IMAGE_PATH}/tmp_${UPDADE_NAME}.img
rm -f ${LINK_IMAGE_PATH}

popd > /dev/null

echo ""
echo "Making updateimg: ${IMAGE_SRC_CUR_PATH}/${UPDADE_NAME}.img"

exit 0
